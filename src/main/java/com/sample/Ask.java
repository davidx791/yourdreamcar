package com.sample;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.eclipse.jdt.internal.core.SetContainerOperation;

public final class Ask extends JFrame{
	
	public static JFrame frame;	
	static private File file;
	private static Image newImage;
	public static int counter;
	
	public static  void init(){
		frame = new JFrame();
        frame.setUndecorated( true );
        frame.setVisible( true );
        frame.setLocationRelativeTo( null );
	}
	
	public static void fDispose(){
		frame.dispose();
	}
	
	public static int chooseOne(String question, String...args){
		int n = JOptionPane.showOptionDialog(null,
			    question,
			    "Pytanie",
			    JOptionPane.YES_NO_CANCEL_OPTION,
			    JOptionPane.QUESTION_MESSAGE,
			    null,
			    args,
			    args[0]);
		return n;
	}
	
	public static void answer(List<Car> suitableCarList){				
		for(int i = 0; i < suitableCarList.size(); i++)
		{
			System.out.println("Samoch�d "+i+": "+suitableCarList.get(i).getBrand()+" "+suitableCarList.get(i).getModel()+" "+
					suitableCarList.get(i).getVersion()+" "+suitableCarList.get(i).getType()+" "+suitableCarList.get(i).getFuel()+" "+
					suitableCarList.get(i).getHorsePower()+" "+suitableCarList.get(i).getImage());	
		}
				
		counter=0;
		JFrame frame= new JFrame();
		frame.setTitle("Wynik ekspertyzy");
		
		//panel glowny 
		JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
 
        //panel informacyjny
        JPanel headingPanel = new JPanel();
        JLabel headingLabel = new JLabel();
        headingPanel.add(headingLabel);
        
        //panel image
        JPanel imgPanel = new JPanel();	
        JLabel imgLabel = new JLabel();
        imgPanel.add(imgLabel);

        //panel grid - info o samochodzie
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints constr = new GridBagConstraints();
        constr.insets = new Insets(0, 0, 15, 15); 
        constr.anchor = GridBagConstraints.EAST;
          
        JLabel brandLabel = new JLabel(" Marka ");
        JLabel modelLabel = new JLabel(" Model ");
        JLabel versionLabel = new JLabel(" Wersja ");
        JLabel typeLabel = new JLabel(" Nadwozie ");
        JLabel fuelLabel = new JLabel(" Silnik ");
        JLabel horsePowerLabel = new JLabel(" Moc ");
                 
        JTextField brandInput = new JTextField(12);
        JTextField modelInput = new JTextField(12);
        JTextField versionInput = new JTextField(12);
        JTextField typeInput = new JTextField(12);
        JTextField fuelInput = new JTextField(12);
        JTextField horsePowerInput = new JTextField(12);
        brandInput.setEditable(false);
        modelInput.setEditable(false);
        versionInput.setEditable(false);
        typeInput.setEditable(false);
        fuelInput.setEditable(false);
        horsePowerInput.setEditable(false);
        
        constr.gridy=0;
        constr.gridx=0; panel.add(brandLabel, constr);
        constr.gridx=1; panel.add(brandInput, constr);
        constr.gridx=2; panel.add(modelLabel, constr);
        constr.gridx=3; panel.add(modelInput, constr);
        
        constr.gridy=1;
        constr.gridx=0; panel.add(versionLabel, constr);
        constr.gridx=1; panel.add(versionInput, constr);
        constr.gridx=2; panel.add(typeLabel, constr);
        constr.gridx=3; panel.add(typeInput, constr);

        constr.gridy=2;
        constr.gridx=0; panel.add(fuelLabel, constr);
        constr.gridx=1; panel.add(fuelInput, constr);
        constr.gridx=2; panel.add(horsePowerLabel, constr);
        constr.gridx=3; panel.add(horsePowerInput, constr);
        
        constr.gridy=3;
        constr.gridx=0; 
        constr.gridwidth = 4;
        constr.anchor = GridBagConstraints.CENTER;
  
        JButton btnNext = new JButton("Nastepny samoch�d");
        btnNext.addActionListener(new ActionListener()
        {
        	public void actionPerformed(ActionEvent e)
          {
        	if(suitableCarList.size() > 0)
        	{
        		headingLabel.setText("Powinienes rozwazyc zakup: ("+(counter+1)+" z "+suitableCarList.size()+")");
        		brandInput.setText(suitableCarList.get(counter).getBrand());
        		modelInput.setText(suitableCarList.get(counter).getModel());
        		versionInput.setText(suitableCarList.get(counter).getVersion());
        		typeInput.setText(suitableCarList.get(counter).getType());
        		fuelInput.setText(suitableCarList.get(counter).getFuel());
        		horsePowerInput.setText(suitableCarList.get(counter).getHorsePower());
        		
        		try {
        			newImage = ImageIO.read(new File(suitableCarList.get(counter).getImage()));
        		} catch (IOException e1) {
        			e1.printStackTrace();
        		}
        		ImageIcon icon = new ImageIcon(newImage);
        		imgLabel.setIcon(icon);
  	        
        		counter++;
        		if (counter == suitableCarList.size())
        			counter = 0;
        	}
        	else
        	{
        		headingLabel.setText("Niestety nie znaleziono pasujacych samochod�w. Spr�buj z innymi kryteriami.");
        		        		
        		try {
        			newImage = ImageIO.read(new File("C:\\Users\\david\\Documents\\Eclipse\\yourDreamCar\\src\\main\\resources\\img\\default.jpg"));
        		} catch (IOException e1) {
        			e1.printStackTrace();
        		}
        		ImageIcon icon = new ImageIcon(newImage);
        		imgLabel.setIcon(icon);
        	 }
        	
        	
  	       }
			
        });
              
        btnNext.doClick();
        if(suitableCarList.size() <= 1)
        	btnNext.setEnabled(false);
        
        panel.add(btnNext,constr);
        
        mainPanel.add(headingPanel);
        mainPanel.add(imgPanel);
        mainPanel.add(panel);

        frame.add(mainPanel);
        frame.pack();
        frame.setSize(500, 500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
		
}


