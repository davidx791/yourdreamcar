package com.sample;

public class Car {

	private String brand;
	private String model;
	private String version;
	private String type;
	private String fuel;
	private String horsePower;
	private String image;
	
	public Car (String _b, String _m, String _v, String _t, String _f, String _hp, String _img)
	{
		brand 	   = _b;
		model 	   = _m;
		version    = _v;
		type 	   = _t;
		fuel 	   = _f;
		horsePower = _hp;
		image 	   = _img;
	}
	
	public String getBrand() 			   		 {return brand;		 }
	public void setBrand(String brand)     		 {this.brand = brand;}
	
	public String getModel() 			   		 {return model;		 }
	public void setModel(String model) 	   		 {this.model = model;}
	
	public String getVersion() 			   		 {return version;		 }
	public void setVersion(String version) 		 {this.version = version;}
	
	public String getType() 			   		 {return type;}
	public void setType(String type) 	  	     {this.type = type;}
	
	public String getFuel() 					 {return fuel;     }
	public void setFuel(String fuel) 			 {this.fuel = fuel;}
	
	public String getHorsePower() 				 {return horsePower;		   }
	public void setHorsePower(String horsePower) {this.horsePower = horsePower;}
	
	public String getImage() 					 {return image;		}
	public void setImage(String img) 			 {this.image = img;	}

}
