package com.sample;

import java.util.ArrayList;
import java.util.List;

import org.kie.api.KieServices;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.KnowledgeBaseFactory;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderError;
import org.kie.internal.builder.KnowledgeBuilderErrors;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.logger.KnowledgeRuntimeLogger;
import org.kie.internal.logger.KnowledgeRuntimeLoggerFactory;
import org.kie.internal.runtime.StatefulKnowledgeSession;


public class DroolsTest {

    public static final void main(String[] args) {
        try {
            KnowledgeBase kbase = readKnowledgeBase();
            StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
            KnowledgeRuntimeLogger logger = KnowledgeRuntimeLoggerFactory.newFileLogger(ksession, "test");
                        
            List<Car> suitableCarList= new ArrayList<Car>();
            ksession.setGlobal("carList", suitableCarList);
            
            Ask.init();
            ksession.fireAllRules();
            Ask.fDispose();
            Ask.answer(suitableCarList);
            
            logger.close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private static KnowledgeBase readKnowledgeBase() throws Exception {
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(ResourceFactory.newClassPathResource("rules/Sample.drl"), ResourceType.DRL);
        KnowledgeBuilderErrors errors = kbuilder.getErrors();
        if (errors.size() > 0) {
            for (KnowledgeBuilderError error: errors) {
                System.err.println(error);
            }
            throw new IllegalArgumentException("Could not parse knowledge.");
        }
        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
        kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
        return kbase;
    }
    
    
    public static class Size {
		private int value;
  	
		public Size(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class SizeS {
		private int value;
		
		public SizeS(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
		}

	public static class SizeM {
		private int value;
  	
		public SizeM(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
    
	public static class SizeL {
		private int value;
		
		public SizeL(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
    
	public static class NumDoors {
		private int value;
  	
		public NumDoors(int n)			{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class FuelPBvsHyb {
		private int value;
  	
		public FuelPBvsHyb(int n)		{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class CombustionPB {
		private int value;
		
		public CombustionPB(int n)		{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class CombustionHyb {
		private int value;
		
		public CombustionHyb(int n)		{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class Company {
		private int value;
		
		public Company(int n)			{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}  
    
    public static class Money {
    	private int value;
      	
    	public Money(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
    
    public static class HP220 {
		private int value;
		
		public HP220(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
    
    public static class ACC6{
		private int value;
		
		public ACC6(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
    
    public static class Clearance {
		private int value;
		
		public Clearance(int n)			{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
    public static class HP200 {
		private int value;
		
		public HP200(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
    
    public static class HP160 {
		private int value;
		
		public HP160(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
    public static class Fuel {
		private int value;
		
		public Fuel(int n)			{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
		
	public static class FuelClassic {
		private int value;
		
		public FuelClassic(int n)		{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class FuelEco {
		private int value;
		
		public FuelEco(int n)			{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class CombustionON {
		private int value;
		
		public CombustionON(int n)		{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	public static class CombustionON2 {
		private int value;
		
		public CombustionON2(int n)		{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
		
	public static class Range{
		private int value;
		
		public Range(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
		
	public static class TrunkSedan {
		private int value;
		
		public TrunkSedan(int n)		{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class TrunkS {
		private int value;
		
		public TrunkS(int n)			{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class ACC9 {
		private int value;
		
		public ACC9(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class Length {
		private int value;
		
		public Length(int n)			{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class EngineCapacity {
		private int value;
		
		public EngineCapacity(int n)	{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	 public static class HP180 {
		private int value;
		
		public HP180(int n)				{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class MaxSpeed {
		private int value;
		
		public MaxSpeed(int n)			{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
	
	public static class Torque {
		private int value;
		
		public Torque(int n)			{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
		
	public static class TrunkVan {
		private int value;
		
		public TrunkVan(int n)			{ value = n;		}
		public int getValue()  			{ return value;		}
		public void setValue(int value) {this.value = value;}
	}
}

